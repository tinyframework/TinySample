/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package spring.integration.ftp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;

public class FTPServerDemo {

	private static String rootDir;
	private static FtpServer ftpServer;

	public static void main(String[] args) throws Exception {
		init();
	}

	public static String initDir() {
		File dir = new File("ftpServer");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String absolutePath = dir.getAbsolutePath();
		absolutePath = absolutePath.replaceAll("\\\\", "/");
		if (!absolutePath.endsWith("/")) {
			absolutePath = absolutePath + "/";
		}
		return absolutePath;
	}

	public static void init() throws Exception {
		rootDir = initDir();
		ftpServer = getFTPServer(rootDir);
		ftpServer.start();
		System.out.println("ftp服务器启动成功，服务器路径" + rootDir);
	}

	private static FtpServer getFTPServer(String ftpRootPath)
			throws IOException {
		File file = new File(ftpRootPath + "users.properties");
		FileOutputStream fos = new FileOutputStream(file);
		PrintWriter pw = new PrintWriter(fos);
		StringBuilder content = new StringBuilder();
		content.append("ftpserver.user.anonymous.homedirectory=")
				.append(ftpRootPath).append("\n");
		content.append("ftpserver.user.anonymous.userpassword=\n");
		content.append("ftpserver.user.anonymous.writepermission=true\n");
		pw.write(content.toString().toCharArray());
		pw.flush();
		if (fos != null) {
			try {
				fos.close();
			} catch (IOException e) {
			}
		}
		if (pw != null) {
			pw.close();
		}

		FtpServerFactory serverFactory = new FtpServerFactory();
		ListenerFactory factory = new ListenerFactory();
		factory.setPort(3333);
		serverFactory.addListener("default", factory.createListener());
		PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
		userManagerFactory.setFile(file);
		serverFactory.setUserManager(userManagerFactory.createUserManager());
		file.delete();
		return serverFactory.createServer();
	}

}
