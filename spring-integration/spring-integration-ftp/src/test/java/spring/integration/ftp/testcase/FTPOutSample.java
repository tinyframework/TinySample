package spring.integration.ftp.testcase;

import java.io.File;

import org.apache.commons.net.ftp.FTPFile;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.outbound.FtpMessageHandler;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class FTPOutSample {
	@Test
	public void testLsGetRm() throws Exception {
		BeanFactory beanFactory = new DefaultListableBeanFactory();
		DefaultFtpSessionFactory ftpSessionFactory = new DefaultFtpSessionFactory();
		ftpSessionFactory.setUsername("anonymous");
		ftpSessionFactory.setPassword("anonymous");
		ftpSessionFactory.setHost("localhost");
		ftpSessionFactory.setPort(3333);
        SessionFactory<FTPFile> sessionFactory = new CachingSessionFactory<FTPFile>(ftpSessionFactory);
		try {
			FtpMessageHandler handler = new FtpMessageHandler(sessionFactory);
			handler.setAutoCreateDirectory(true);
			handler.setRemoteDirectoryExpression(new LiteralExpression("/ss"));
	        handler.setFileNameGenerator(new FileNameGenerator() {

	            public String generateFileName(Message<?> message) {
	                 return ((File) message.getPayload()).getName();
	            }

	        });
	        handler.setBeanFactory(beanFactory);
			handler.afterPropertiesSet();
	        File fileToSendA = new File("F:/Demo/demonew/protocol/org.tinygroup.protocol.sample/spring-integration-ftp/ftpClient/a.txt");
	        handler.handleMessage(new GenericMessage<File>(fileToSendA));
//			FileTransferringMessageHandler<FTPFile> handler = new FileTransferringMessageHandler<FTPFile>(sessionFactory);
//			handler.setRemoteDirectoryExpression(new LiteralExpression("F:/Demo/demonew/protocol/org.tinygroup.protocol.sample/spring-integration-ftp/ftpServer"));
//			handler.setFileNameGenerator(new FileNameGenerator() {
//				public String generateFileName(Message<?> message) {
//					return ((File) message.getPayload()).getName();
//				}
//			});
//			handler.setBeanFactory(beanFactory);
//			handler.afterPropertiesSet();
//			File fileToSendA = new File("F:/Demo/demonew/protocol/org.tinygroup.protocol.sample/spring-integration-ftp/ftpClient/a.txt");
//			Message<File> messageA = MessageBuilder.withPayload(fileToSendA)
//					.build();
//			handler.handleMessage(new GenericMessage<File>(fileToSendA));
	//		handler.handleMessage(new GenericMessage<String>("String data"));
	//		byte[] inFile = FileCopyUtils.copyToByteArray(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}