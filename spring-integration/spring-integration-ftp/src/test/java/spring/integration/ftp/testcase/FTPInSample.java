package spring.integration.ftp.testcase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPFile;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.FileListFilter;
import org.springframework.integration.ftp.filters.FtpRegexPatternFileListFilter;
import org.springframework.integration.ftp.inbound.FtpInboundFileSynchronizer;
import org.springframework.integration.ftp.inbound.FtpInboundFileSynchronizingMessageSource;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.messaging.Message;

public class FTPInSample {
	private static final Logger LOGGER = LoggerFactory.getLogger(FTPInSample.class);

	@Test
	public void testLsGetRm() throws Exception {
		DefaultFtpSessionFactory ftpSessionFactory = new DefaultFtpSessionFactory();
		ftpSessionFactory.setUsername("anonymous");
		ftpSessionFactory.setPassword("anonymous");
		ftpSessionFactory.setHost("localhost");
		ftpSessionFactory.setPort(3333);
		
		FtpInboundFileSynchronizer synchronizer = new FtpInboundFileSynchronizer(ftpSessionFactory);
		synchronizer.setDeleteRemoteFiles(false);
		synchronizer.setPreserveTimestamp(true);
		synchronizer.setRemoteDirectory("/a");
		FtpRegexPatternFileListFilter patternFilter = new FtpRegexPatternFileListFilter("a.txt");
//		PropertiesPersistingMetadataStore store = new PropertiesPersistingMetadataStore();
//		store.setBaseDirectory("test");
//		store.afterPropertiesSet();
//		FtpPersistentAcceptOnceFileListFilter persistFilter =
//				new FtpPersistentAcceptOnceFileListFilter(store, "foo");
		List<FileListFilter<FTPFile>> filters = new ArrayList<FileListFilter<FTPFile>>();
//		filters.add(persistFilter);
		filters.add(patternFilter);
		CompositeFileListFilter<FTPFile> filter = new CompositeFileListFilter<FTPFile>(filters);
		synchronizer.setFilter(filter);

//		ExpressionParser expressionParser = new SpelExpressionParser(new SpelParserConfiguration(true, true));
//		Expression expression = expressionParser.parseExpression("#this.toUpperCase() + '.a'");
//		synchronizer.setLocalFilenameGeneratorExpression(expression);
		BeanFactory beanFactory = new DefaultListableBeanFactory();
		synchronizer.setBeanFactory(beanFactory);
		synchronizer.afterPropertiesSet();
		FtpInboundFileSynchronizingMessageSource ms = new FtpInboundFileSynchronizingMessageSource(synchronizer);
		ms.setAutoCreateLocalDirectory(true);
		File localFile = new File("ftpClient");
		ms.setLocalDirectory(localFile);
		ms.setBeanFactory(beanFactory);
		
//		CompositeFileListFilter<File> localFileListFilter = new CompositeFileListFilter<File>();
//		localFileListFilter.addFilter(new RegexPatternFileListFilter(".*\\.TXT\\.a$"));
//		AcceptOnceFileListFilter<File> localAcceptOnceFilter = new AcceptOnceFileListFilter<File>();
//		localFileListFilter.addFilter(localAcceptOnceFilter);
//		ms.setLocalFilter(localFileListFilter);
		
		ms.afterPropertiesSet();
		Message<File> atestFile =  ms.receive();
		LOGGER.info(String.format("Received first file message: %s.", atestFile));
				
		ms.stop();
	}

}