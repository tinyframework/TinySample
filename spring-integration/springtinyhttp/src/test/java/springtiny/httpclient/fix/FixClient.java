package springtiny.httpclient.fix;

import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import springtiny.httpclient.RequestGateway;

import com.hundsun.fasp.trans.fix.base.FixData;

/**
 * 将整个工程当做web应用启动，然后再启动本main函数
 *
 */
public class FixClient {
	private static Logger logger = Logger.getLogger(FixClient.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"/FixClient.xml");
		RequestGateway requestGateway = context.getBean("requestGateway",
				RequestGateway.class);
		FixData fd = (FixData) requestGateway.send(getFixData());
		logger.info("\n\n++++++++++++ Replied with: " + fd.getColumnCount()
				+ " ++++++++++++\n");
	}

	public static FixData getFixData() {
		FixData fd = new FixData();
		fd.addColumn("a", "String", true);
		fd.addColumn("b", "String", true);
		fd.addColumn("c", "String", true);
		fd.addColumn("d", "String", true);
		fd.addColumn("e", "String", true);
		return fd;
	}
}
