package springtiny.httpclient.p8583;

import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import springtiny.httpclient.RequestGateway;

/**
 * 将整个工程当做web应用启动，然后再启动本main函数
 * 
 */
public class P8583Client {

	private static Logger logger = Logger.getLogger(P8583Client.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"/P8583Client.xml");
		RequestGateway requestGateway = context.getBean("requestGateway",
				RequestGateway.class);
		TreeMap filedMap = new TreeMap();// 报文域
		filedMap.put("FD128", "100001");// 交易码
		filedMap.put("FD118", "1105");// 交易码
		filedMap.put("FD122", "该客户号对应的客户销售商签约信息不存在");// 交易码
		filedMap.put("FD112", "20131106");// 交易日期
		filedMap.put("FD110", "130005");// 系统时间
		TreeMap tm = (TreeMap) requestGateway.send(filedMap);
		logger.info("\n\n++++++++++++ Replied with: " + tm.size()
				+ " ++++++++++++\n");
	}

}
