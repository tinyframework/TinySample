package springtiny.httpclient.string;

import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import springtiny.User;
import springtiny.httpclient.RequestGateway;

public class TemplateClient {

	private static Logger logger = Logger.getLogger(TemplateClient.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"/TemplateClient.xml");
		RequestGateway requestGateway = context.getBean("requestGateway",
				RequestGateway.class);
		User u = new User();
		u.setName("chendashen");
		User user = (User) requestGateway.send(u);
		logger.info("\n\n++++++++++++ Replied with: " + user.toString()
				+ " ++++++++++++\n");
	}

}
