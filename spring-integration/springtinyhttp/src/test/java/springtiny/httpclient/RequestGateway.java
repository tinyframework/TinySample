package springtiny.httpclient;


public interface RequestGateway {

	Object send(Object obj);

}
