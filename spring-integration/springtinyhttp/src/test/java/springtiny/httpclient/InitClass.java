package springtiny.httpclient;

import java.io.InputStream;

import org.tinygroup.trans.template.manager.TemplateConvertManager;
import org.tinygroup.xstream.XStreamFactory;
import org.tinygroup.xstream.config.XStreamConfiguration;

import com.thoughtworks.xstream.XStream;

public class InitClass {

	public void init() {
		TemplateConvertManager
				.put("src/test/resources/object-xml.templateconvert.xml");
		init("/user.xstream.xml");
	}

	public XStream init(String path) {
		XStream stream = new XStream();
		stream.autodetectAnnotations(true);
		stream.alias("xstream-configuration", XStreamConfiguration.class);
		InputStream inputStream = InitClass.class.getResourceAsStream(path);
		XStreamConfiguration xStreamConfiguration = (XStreamConfiguration) stream
				.fromXML(inputStream);
		try {
			XStreamFactory.parse(xStreamConfiguration);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return XStreamFactory.getXStream(xStreamConfiguration.getPackageName());
	}
}
