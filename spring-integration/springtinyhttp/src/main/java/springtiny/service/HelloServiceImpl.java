package springtiny.service;

import java.util.TreeMap;

import springtiny.User;

import com.hundsun.fasp.trans.fix.base.FixData;

public class HelloServiceImpl implements HelloService {

	public User hello(User user) {
		user.setAge(18);
		user.setGrade("8");
		return user;
	}

	public User hahaha(String name, int age) {
		User user = new User();
		user.setName(name);
		user.setAge(age);
		user.setGrade("8");
		throw new RuntimeException("出错啦");
	}

	public FixData helloFix(FixData fixData) {
		FixData fd = new FixData();
		fd.addColumn("a", "String", true);
		fd.addColumn("b", "String", true);
		fd.addColumn("c", "String", true);
		fd.addColumn("d", "String", true);
		fd.addColumn("e", "String", true);
		return fd;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public TreeMap hello8583(TreeMap tm) {
		TreeMap filedMap = new TreeMap();// 报文域
		filedMap.put("FD128", "100001");// 交易码
		filedMap.put("FD118", "1105");// 交易码
		filedMap.put("FD122", "该客户号对应的客户销售商签约信息不存在");// 交易码
		filedMap.put("FD112", "20131106");// 交易日期
		filedMap.put("FD110", "130005");// 系统时间
		return filedMap;
	}
}
