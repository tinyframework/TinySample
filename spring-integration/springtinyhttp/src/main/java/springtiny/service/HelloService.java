package springtiny.service;

import java.util.TreeMap;

import springtiny.User;

import com.hundsun.fasp.trans.fix.base.FixData;

public interface HelloService {

	public User hello(User user);

	public User hahaha(String name, int age);

	public FixData helloFix(FixData user);

	@SuppressWarnings("rawtypes")
	public TreeMap hello8583(TreeMap user);
}
