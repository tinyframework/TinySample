package springtiny.service;

import org.springframework.messaging.Message;

public class HttpCommonTrans {
	/**
	 * 输入对象转换
	 * 
	 * @param inMessage
	 * @return
	 */
	public Message<?> commonTransInt(Message<?> inMessage) {
		return inMessage;
	}

	/**
	 * 输出对象转换
	 * 
	 * @param inMessage
	 * @return
	 */
	public Message<?> commonTransOut(Message<?> inMessage) {
		return inMessage;
	}

}
