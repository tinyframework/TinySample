package springtiny.service;

import java.util.List;
import java.util.Map;

import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class UriVariablesBean {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transformer
	public Message<Object> transforIn(Message<?> inMessage) {
		Map payload = (Map) inMessage.getPayload();
		List<Object> list = (List<Object>) payload.get("var");
		Message<Object> message = new GenericMessage<Object>(list.get(0),
				inMessage.getHeaders());
		return message;
	}
	
	public Message<Object> transforError(Message<?> inMessage) {
		String xml = "<user><name>error</name></user>";
		Message<Object> message = new GenericMessage<Object>(xml,
				inMessage.getHeaders());
		return message;
	}
}
