package springtiny;

import java.io.Serializable;

@SuppressWarnings("serial")
public class User implements Serializable{
	String name;
	int age;
	String grade;
	boolean response = false;

	public User() {
	}

	public User(String name, int age, String grade) {
		super();
		this.name = name;
		this.age = age;
		this.grade = grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return name + "#" + age + "#" + grade;
	}
}
