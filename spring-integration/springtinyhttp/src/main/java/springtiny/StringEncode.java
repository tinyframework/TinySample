package springtiny;

import org.springframework.core.convert.converter.Converter;

public class StringEncode implements Converter<String, byte[]> {

	@Override
	public byte[] convert(String source) {
		return source.getBytes();
	}

}
