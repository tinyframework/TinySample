package springintegrationsample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

import com.hundsun.fasp.trans.fix.base.FixData;
import com.hundsun.fasp.trans.fix.base.HsFix;
import com.hundsun.fasp.trans.fix.base.util.DataSet2Fix;
import com.hundsun.fasp.trans.fix.base.util.Fix2DataSet;
import com.hundsun.fasp.trans.fix.base.util.FixAnalyzer;
import com.hundsun.fasp.trans.fix.base.util.FixUtil;

public class FixDataDeserializer implements Serializer<FixData>, Deserializer<FixData> {
	private static int HEAD_LENGTH_BYTES_LENGTH = 3;
	private static int CHARSET_LENGTH = 8;
	private static String charset = "GBK";

	@Override
	public FixData deserialize(InputStream inputStream) throws IOException {
		FixData analyzerMakeFix = new FixData();
		analyzerMakeFix = parse(inputStream);
		return analyzerMakeFix;
	}
	private static int parseOrderNumber(InputStream inputStream,int length) throws IOException {
		String value = parseString(inputStream, length);
		return Integer.valueOf(value.toString());
	}
	
	private static String parseString(InputStream inputStream, int length) throws IOException {
		StringBuilder builder = new StringBuilder();
		int c;
		for (int i = 0; i < length; ++i) {
			c = inputStream.read();
			checkClosure(c);
			builder.append((char)c);
		}

		return builder.toString();
	}

	protected static void checkClosure(int bite) throws IOException {
		if (bite < 0) {
//			logger.debug("Socket closed during message assembly");
			throw new IOException("Socket closed during message assembly");
		}
	}
	
	public static FixData parse(InputStream inputStream) throws IOException {
		
		int c = inputStream.read();
		checkClosure(c);
		int headLength  = parseOrderNumber(inputStream,HEAD_LENGTH_BYTES_LENGTH);
		if (headLength < 9) {
			throw new RuntimeException("报文太短");
		}
		int contentLength =  parseOrderNumber(inputStream,5);
		
		if (headLength >= 17) {
			charset = parseString(inputStream, 8);
		}
		if (charset.equals("") || (!charset.equals("GBK") && !charset.equals("UTF-8") && !charset.equals("GB18030"))) {
			charset = "GBK";
		}
		byte[] contextBytes = new byte[contentLength];
		inputStream.read(contextBytes, 0, contentLength);
		FixData analyzerMakeFix = Fix2DataSet.fix2DataSet(new String(contextBytes, charset));
		return analyzerMakeFix;
	}

	@Override
	public void serialize(FixData fixData, OutputStream outputStream) throws IOException {
		byte[] pack = make(fixData);
		outputStream.write(pack);
		outputStream.flush();
	}

	public static byte[] make(FixData fixData) throws IOException {
		HsFix fix = DataSet2Fix.data2Fix(fixData);
		byte[] content = fix.toString().getBytes(charset);
		// TODO:拼接报文头
		int headLength = 1 + 3 + 5 + ((charset.equals("GBK") || charset.equals("GB18030")) ? 0 : CHARSET_LENGTH);
		byte[] pack = new byte[headLength + content.length];
		System.arraycopy(("F" + FixUtil.fix0BeforeString(String.valueOf(headLength), 3)
				+ FixUtil.fix0BeforeString(String.valueOf(content.length), 5)
				+ ((charset.equals("GBK") || charset.equals("GB18030")) ? ""
						: (FixUtil.fixSpaceAfterString(charset, 8)))).getBytes(),
				0, pack, 0, headLength);
		System.arraycopy(content, 0, pack, headLength, content.length);

		return pack;

	}
	
	
}
