package springintegrationsample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.core.serializer.DefaultDeserializer;
import org.springframework.core.serializer.DefaultSerializer;
import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

public class FixDataDeserializer2 implements Serializer<Object>, Deserializer<Object> {
	DefaultSerializer ds = new DefaultSerializer();
	DefaultDeserializer dd = new DefaultDeserializer();
	@Override
	public Object deserialize(InputStream inputStream) throws IOException {
		// TODO Auto-generated method stub
		return dd.deserialize(inputStream);
	}

	@Override
	public void serialize(Object object, OutputStream outputStream) throws IOException {
		ds.serialize(object, outputStream);
	}


	
	
}
