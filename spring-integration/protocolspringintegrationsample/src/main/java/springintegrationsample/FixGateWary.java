package springintegrationsample;

import com.hundsun.fasp.trans.fix.base.FixData;

public interface FixGateWary {
	public FixData send(FixData text);
}
