package springintegrationsample;


public class StringService {
	public String helloFixData(String fix){
		System.out.println("---------execute-----------");
		System.out.println("---------"+fix+"-----------");
		return fix;
	}
	
	public String sleepString(String fix) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return fix+":chendashen";
	}
}
