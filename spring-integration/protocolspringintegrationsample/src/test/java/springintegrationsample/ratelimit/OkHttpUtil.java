package springintegrationsample.ratelimit;

import org.springframework.integration.samples.tcpclientserver.SimpleGateway;

public class OkHttpUtil {

	public static void run(SimpleGateway requestGateway) {
		String s = requestGateway.send("a");
		System.out.println(s);
	}

}