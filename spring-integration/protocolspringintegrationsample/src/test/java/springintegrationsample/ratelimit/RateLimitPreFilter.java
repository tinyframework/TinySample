package springintegrationsample.ratelimit;

import com.hundsun.fasp.trans.fix.base.FixData;

public class RateLimitPreFilter {
	public boolean check(FixData o){
		
		if(o.getRowCount()>0){
			return false;
		}
		System.out.println("RateLimitPreFilter");
		return true;
	}
}
