package springintegrationsample;

import java.io.IOException;

import org.springframework.core.convert.converter.Converter;

import com.hundsun.fasp.trans.fix.base.FixData;

public class FixConvert2 implements Converter<FixData,byte[] >{
	private static String charset = "GBK";

	@Override
	public byte[] convert(FixData source) {
		try {
			return FixDataDeserializer.make(source);
		} catch (IOException e) {
			return null;
		}
	}
	
}
