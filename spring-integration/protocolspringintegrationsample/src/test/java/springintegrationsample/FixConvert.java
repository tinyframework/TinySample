package springintegrationsample;

import org.springframework.core.convert.converter.Converter;

import com.hundsun.fasp.trans.fix.base.FixData;
import com.hundsun.fasp.trans.fix.base.util.FixAnalyzer;

public class FixConvert implements Converter<byte[], FixData>{
	private static String charset = "GBK";
	public FixData convert(byte[] source) {
		// TODO Auto-generated method stub
		try {
			return decode(source);
		} catch (Exception e) {
			return null;
		}
	}
	
	public FixData decode(byte[] by) throws Exception {
		byte[] length = new byte[3];
		System.arraycopy(by, 1, length, 0, 3);
		int headLength = Integer.parseInt(new String(length, 0, 3));
		if (headLength < 9) {
			return null;
		}
		byte[] content = new byte[5];
		System.arraycopy(by, 4, content, 0, 5);
		int contentLength = Integer.parseInt(new String(content, 1, 4));
		if (headLength >= 17) {
			byte[] charsetBy = new byte[8];
			System.arraycopy(by, 9, charsetBy, 0, 8);
			charset = new String(charsetBy, 0, 8).trim().toUpperCase();
		}
		byte[] body = new byte[contentLength];
		System.arraycopy(by, headLength, body, 0, contentLength);
		return FixAnalyzer.analyzerMakeFix(body, charset);
	}

}
