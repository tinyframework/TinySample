package org.tinygroup.mqsample;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.tinygroup.mqsample.gateway.MqGateWay;
import org.tinygroup.mqsample.model.Message;

public class SendAndReceiveTest {
	private static ApplicationContext applicationContext;

	private MqGateWay mqGateWay;

	@BeforeClass
	public static void setupClass() {
		// applicationContext = new ClassPathXmlApplicationContext(
		// "WEB-INF/springintegrationexample-servlet.xml" );

		applicationContext = new FileSystemXmlApplicationContext(
				"src/main/webapp/WEB-INF/clientwithreturn.xml");
	}

	@Before
	public void setup() {
		mqGateWay = applicationContext.getBean(MqGateWay.class);
	}


//	@Test
	public void testSendAndReceiveMessage() throws Exception {

		String message = mqGateWay.sendAndReceiveMessage(new Message("Test System",
				Message.MessageType.CREATE, "This is my message"));
		
		System.out.println(message);

	}
	
}
