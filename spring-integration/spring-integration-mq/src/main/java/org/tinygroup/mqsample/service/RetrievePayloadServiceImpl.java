package org.tinygroup.mqsample.service;

import org.springframework.stereotype.Service;
import org.tinygroup.mqsample.model.Message;

@Service
public class RetrievePayloadServiceImpl implements RetrievePayloadService
{
    public String getPayload(Message message)
    {
        // Go back to the SOR and retrieve the payload for the specified id ...
        return "Payload for " + message.toString();
    }
}
