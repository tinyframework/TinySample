package org.tinygroup.mqsample.listener;

public interface MessageListener
{
    public void processMessage( String message );
}
