package org.tinygroup.mqsample.gateway;

import org.tinygroup.mqsample.model.Message;

/**
 * mq消息网关
 * @author renhui
 *
 */
public interface MqGateWay {
	
	public void sendMessage(Message message);
	
	public String sendAndReceiveMessage(Message message);
}
