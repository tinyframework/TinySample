package org.tinygroup.mqsample.service;

import org.tinygroup.mqsample.model.Message;

public interface RetrievePayloadService
{
    public String getPayload( Message message );
}
