package org.tinygroup.mqsample.service;

import org.tinygroup.mqsample.model.Message;

public interface PublishService
{
    public void send( Message message );
}
