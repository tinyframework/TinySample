package org.tinygroup.mqsample.listener;

import org.springframework.stereotype.Service;

@Service
public class ReceiveMessageListener {
	
	public String processMessage(String message){
		
		return "this is response message:"+message;
	}

}
