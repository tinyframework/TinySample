package org.tinygroup.mqsample.service;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
public class MqResponseHandler {
	
	public String responseHandler(Message<String> message){
		 return message.getPayload();
	}

}
