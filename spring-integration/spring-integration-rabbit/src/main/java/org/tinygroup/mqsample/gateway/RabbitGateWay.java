package org.tinygroup.mqsample.gateway;

import org.tinygroup.mqsample.model.Message;

/**
 * rabbitmq消息网关
 * @author renhui
 *
 */
public interface RabbitGateWay {
	
	public void sendMessage(Message message);
	
	public String sendAndReceiveMessage(Message message);
}
