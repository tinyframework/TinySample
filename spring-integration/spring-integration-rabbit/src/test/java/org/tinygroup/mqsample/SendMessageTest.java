package org.tinygroup.mqsample;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.tinygroup.mqsample.gateway.RabbitGateWay;
import org.tinygroup.mqsample.model.Message;

public class SendMessageTest {
	private static ApplicationContext applicationContext;

	private RabbitGateWay mqGateWay;

	@BeforeClass
	public static void setupClass() {
		// applicationContext = new ClassPathXmlApplicationContext(
		// "WEB-INF/springintegrationexample-servlet.xml" );

		applicationContext = new FileSystemXmlApplicationContext(
				"src/main/webapp/WEB-INF/client.xml");
	}

	@Before
	public void setup() {
		mqGateWay = applicationContext.getBean(RabbitGateWay.class);
	}


//	@Test
	public void testMqGateWay() throws InterruptedException {

		mqGateWay.sendMessage(new Message("Test System",
				Message.MessageType.CREATE, "This is my message"));
		Thread.sleep(3000);

	}

	
}
