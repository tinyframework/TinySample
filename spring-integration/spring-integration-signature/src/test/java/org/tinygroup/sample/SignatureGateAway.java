package org.tinygroup.sample;

/**
 * 
 * 进行签名验证的网关接口
 * @author renhui
 *
 */
public interface SignatureGateAway {

	public boolean sinature(String xml);
	
}
