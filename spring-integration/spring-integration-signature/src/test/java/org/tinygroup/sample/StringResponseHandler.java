package org.tinygroup.sample;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;

@Component
public class StringResponseHandler {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public boolean getResponse(Message<String> msg) {
		logger.info("Building request for course [{}]", msg.getPayload());
		return msg.getPayload()!=null;
	}
}