package org.tinygroup.sample;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 加签验签成功测试用例
 * 
 * @author renhui
 *
 */
@ContextConfiguration({ "classpath:/META-INF/spring/integration/success/signsuccess.xml" })
//@RunWith(SpringJUnit4ClassRunner.class)
public class TestSignatureCheckSuccess {

	@Autowired
	SignatureGateAway signatureGateAway;

//	@Test
	public void testCheckSuccess() {

		boolean success = signatureGateAway
				.sinature("<?xml version=\"1.0\"?><getCourseRequest xmlns=\"http://www.xpadro.spring.samples.com/courses\" courseId=\"BC-45\"></getCourseRequest>");

		assertTrue(success);
	}

}
