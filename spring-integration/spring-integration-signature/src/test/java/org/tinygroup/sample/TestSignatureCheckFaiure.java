package org.tinygroup.sample;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageDeliveryException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 加签验签失败测试用例
 * 
 * @author renhui
 *
 */
@ContextConfiguration({ "classpath:/META-INF/spring/integration/success/signfailure.xml" })
//@RunWith(SpringJUnit4ClassRunner.class)
public class TestSignatureCheckFaiure {

	@Autowired
	SignatureGateAway signatureGateAway;

//	@Test(expected=MessageDeliveryException.class)
	public void testCheckSuccess() {

		signatureGateAway
				.sinature("<?xml version=\"1.0\"?><getCourseRequest xmlns=\"http://www.xpadro.spring.samples.com/courses\" courseId=\"BC-45\"></getCourseRequest>");

	}

}
