package org.tinygroup.signature;

import org.springframework.core.io.Resource;

/**
 * XML数字签名配置项
 *
 * @author renhui
 */
public class XmlSignatureConfig {

	// 使用者ID
	private String userId;

	// 私钥文件路径
	private Resource privateKey;

	// 私钥存储类型
	private String privateStoreType;

	// 公钥证书路径
	private Resource publicKey;

	// 公钥存储类型
	private String publicStoreType;

	// 别名
	private String alias;

	// 密码
	private String password;
	
	private String mechanismType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Resource getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(Resource privateKey) {
		this.privateKey = privateKey;
	}

	public Resource getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(Resource publicKey) {
		this.publicKey = publicKey;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrivateStoreType() {
		return privateStoreType;
	}

	public void setPrivateStoreType(String privateStoreType) {
		this.privateStoreType = privateStoreType;
	}

	public String getPublicStoreType() {
		return publicStoreType;
	}

	public void setPublicStoreType(String publicStoreType) {
		this.publicStoreType = publicStoreType;
	}

	
	public String getMechanismType() {
		return mechanismType;
	}

	public void setMechanismType(String mechanismType) {
		this.mechanismType = mechanismType;
	}

	public String toString() {
		return "XmlSignatureConfig [userId=" + userId + ", privateKeyPath="
				+ privateKey + ", privateStoreType=" + privateStoreType
				+ ", publicKeyPath=" + publicKey + ", publicStoreType="
				+ publicStoreType + ", alias=" + alias + ", password="
				+ password + ",mechanismType="+mechanismType+"]";
	}

}