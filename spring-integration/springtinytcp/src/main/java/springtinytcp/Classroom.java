package springtinytcp;

public class Classroom extends BaseEntity {
	private static final long serialVersionUID = 5871208751874068123L;
	private String roomCode;
	private String buildingName;
	private Integer peopleCapacity;

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode == null ? null : roomCode.trim();
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName == null ? null : buildingName.trim();
	}

	public Integer getPeopleCapacity() {
		return peopleCapacity;
	}

	public void setPeopleCapacity(Integer peopleCapacity) {
		this.peopleCapacity = peopleCapacity;
	}
}
