package springtinytcp;

import java.io.Serializable;

public class BaseEntity implements Serializable{
	private static final long serialVersionUID = -1649090802368156758L;

	private long id;
	
	private String base;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}
	
}
