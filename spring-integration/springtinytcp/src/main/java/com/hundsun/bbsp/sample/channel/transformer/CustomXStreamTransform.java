package com.hundsun.bbsp.sample.channel.transformer;

import com.thoughtworks.xstream.XStream;
import org.springframework.messaging.Message;
import org.tinygroup.trans.xstream.util.XStreamConvertUtil;
import org.tinygroup.xstream.XStreamFactory;

public class CustomXStreamTransform {
	private String xstreamPackageName;

	public String getXstreamPackageName() {
		return xstreamPackageName;
	}

	public void setXstreamPackageName(String xstreamPackageName) {
		this.xstreamPackageName = xstreamPackageName;
	}

	/**
	 * xml转对象，采用xstream的方式
	 * 
	 * @param source
	 * @return
	 */
	public Object transfor2Object(String source) {
		String xmlPattern = source.substring(source.indexOf(" ") + 1);
		System.out.println("xml转对象，xml：" + xmlPattern);
		XStream xStream = XStreamFactory.getXStream(xstreamPackageName);
		return XStreamConvertUtil.xml2Object(xStream, xmlPattern);
	}

	/**
	 * 对象转xml，采用xstream的方式
	 * 
	 * @param inMessage
	 * @return
	 */
	public String transfor2Xml(Message<?> inMessage) {
		XStream xStream = XStreamFactory.getXStream(xstreamPackageName);
		String xmlPattern = XStreamConvertUtil.object2Xml(xStream, inMessage.getPayload());
		System.out.println("对象转xml，转换之后xml：" + xmlPattern);
		return xmlPattern;
	}

}
