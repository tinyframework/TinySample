package com.hundsun.bbsp.sample.channel.transformer;

import org.springframework.core.convert.converter.Converter;

public class StringEncode implements Converter<String, byte[]> {

	public byte[] convert(String source) {
		return source.getBytes();
	}

}
