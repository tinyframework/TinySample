package com.hundsun.bbsp.sample.channel.transformer;

import org.springframework.messaging.MessageHandlingException;

import springtinytcp.Classroom;

public class ErrorTransfrom {

	public Object transforError(MessageHandlingException exception) {
		Throwable throwable = exception.getCause();
		System.out.println(throwable);
		System.out.println("服务执行异常，返回异常报文");
		Classroom classroom = new Classroom();
		classroom.setId(12345);
//		classroom.setRoomCode("我是错误码");
		classroom.setRoomCode(throwable.toString());
		return classroom;
	}
}
