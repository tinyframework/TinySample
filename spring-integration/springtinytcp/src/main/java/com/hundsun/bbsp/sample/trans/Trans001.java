package com.hundsun.bbsp.sample.trans;

import springtinytcp.Classroom;

/**
 * Created by wangwy on 2017/4/19.
 */
public class Trans001 {
	public Classroom execute(Classroom classroom) {
		System.out.println("执行服务第一步，只有一个参数");
		return classroom;
	}

	public Classroom execute(long id, String roomCode) {
		System.out.println("执行服务第二步，有多个参数");
		Classroom classroom = new Classroom();
		classroom.setId(id);
		classroom.setRoomCode("1001");
		return classroom;
	}
	
	public Classroom throwError(long id, String roomCode) {
		System.out.println("执行服务第三步，服务抛出异常");
		throw new RuntimeException("服务执行出错啦！");
	}
}
