package com.hundsun.bbsp.sample.channel.transformer;

import org.springframework.core.convert.converter.Converter;

public class StringDecode implements Converter<byte[], String> {
	private static String charset = "UTF-8";

	public String convert(byte[] source) {
		try {
			return decode(source);
		} catch (Exception e) {
			return null;
		}
	}

	public String decode(byte[] by) throws Exception {
		return new String(by, charset);
	}

}
