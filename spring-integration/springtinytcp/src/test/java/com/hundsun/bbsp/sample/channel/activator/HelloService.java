package com.hundsun.bbsp.sample.channel.activator;


import springtinytcp.Classroom;
import springtinytcp.PageResponse;

public interface HelloService {

	public PageResponse<Classroom> getPage(Classroom classroom);
}
