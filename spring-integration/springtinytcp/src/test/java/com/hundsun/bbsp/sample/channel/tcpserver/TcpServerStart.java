package com.hundsun.bbsp.sample.channel.tcpserver;

import org.tinygroup.tinyrunner.Runner;

/**
 * Created by wangwy on 2017/4/17.
 */
public class TcpServerStart {
	public static void main(String[] args) throws InterruptedException {
		Runner.init("application.xml", null);
	}
}
