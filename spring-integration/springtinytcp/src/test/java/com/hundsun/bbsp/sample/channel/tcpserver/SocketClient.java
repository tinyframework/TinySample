package com.hundsun.bbsp.sample.channel.tcpserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Created by wangwy on 2017/4/17.
 */
public class SocketClient {
	public static void main(String[] args) throws IOException {
		// 客户端请求与本机在7101端口建立TCP连接
		Socket client = new Socket("127.0.0.1", 7101);
		client.setSoTimeout(10000);
		String str1 = "001 <classroom></classroom>";
		// 获取Socket的输出流，用来发送数据到服务端
		PrintStream out = new PrintStream(client.getOutputStream());
		out.println(str1);
		// 获取Socket的输入流，用来接收从服务端发送过来的数据
		InputStream is = client.getInputStream();
		byte[] bufs = new byte[9999];
		// 注意：read会产生阻塞
		is.read(bufs);
		System.out.println(new String(bufs));
		client.close();
	}
}
