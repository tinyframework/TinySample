package springintegrationsample.test.runner;

import org.junit.Assert;
import org.tinygroup.beancontainer.BeanContainer;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.cepcore.CEPCore;
import org.tinygroup.cepcore.EventProcessor;
import org.tinygroup.context.Context;
import org.tinygroup.context.util.ContextFactory;
import org.tinygroup.event.Event;
import org.tinygroup.tinyrunner.Runner;

import springintegrationsample.eventprocessor.SimpleService;
import springintegrationsample.eventprocessor.SpringTcpEventProcessor;
import springintegrationsample.test.util.DataUtil;

import com.hundsun.fasp.trans.fix.base.FixData;

/**
 * 先启动TcpDemoWithConversionServer（做为服务端）
 * 再启动本main函数
 * 演示如何将外部通道对接到Tiny CEPCore
 * 外部通道，通过实现接口EventProcessor，对接到CEPCore
 * 该方式将一系列服务号与该通道进行映射
 * CEPCore收到该服务号对应的服务调用，会将其路由到对应EventProcessor上执行。
 * @author chenjiao
 *
 */
public class TestChannelAsEventProcessor {
	private static final String SERVICEA = "servicea";

	public static void main(String[] args) {
		Runner.init(null, null); // 启动框架
		BeanContainer<?> c = BeanContainerFactory.getBeanContainer(TestChannelAsEventProcessor.class.getClassLoader());
		EventProcessor processor = (EventProcessor) c.getBean(SpringTcpEventProcessor.BEAN_ID);
		processor.getServiceInfos().add(new SimpleService(SERVICEA));
		CEPCore cepCore = (CEPCore) c.getBean(CEPCore.CEP_CORE_BEAN);
		cepCore.registerEventProcessor(processor);

		Context context = ContextFactory.getContext();
		context.put(SimpleService.PARAM_NAME, DataUtil.getFixData(1));
		cepCore.process(Event.createEvent(SERVICEA, context));
		FixData data = context.get(SimpleService.RESULT_NAME);
		Assert.assertEquals(5, data.getColumnCount());
		System.out.println("===================================");
		System.out.print("data.getColumnCount():");
		System.out.println(data.getColumnCount());
		System.out.println("===================================");
	}

}
