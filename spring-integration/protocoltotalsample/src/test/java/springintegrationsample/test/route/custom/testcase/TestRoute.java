package springintegrationsample.test.route.custom.testcase;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import springintegrationsample.CustomTestContextLoader;
import springintegrationsample.test.route.custom.HeaderService;

/**
 * SpringIntergration自身提供的路由方案，此用例是基于请求头进行路由
 * 通过header中的属性type的值来进行通道路由
 * @author chenjiao
 *
 */
@ContextConfiguration(loader = CustomTestContextLoader.class, locations = { "/route/headerroute.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
public class TestRoute {
	private static final String HEAD_KEY = "type";
	@Autowired
	MessageChannel channel;
	
	
	@Before
	public void setup() {
	}

	@Test
	public void testRoute1() {
		String payload = "valuemq";
		String channelType = "mq";
		Message<String> message = MessageBuilder.withPayload(payload).setHeader(HEAD_KEY, channelType).build();
		channel.send(message);
		assertEquals(payload, HeaderService.MQ_NAME);
	}
	@Test
	public void testRoute2() {
		
		String payload = "valuetcp";
		String channelType = "tcp";
		Message<String> message = MessageBuilder.withPayload(payload).setHeader(HEAD_KEY, channelType).build();
		channel.send(message);
		assertEquals(payload, HeaderService.TCP_NAME);
	}
	@Test
	public void testRoute3() {
		String payload = "valuewebservice";
		String channelType = "webservice";
		Message<String> message = MessageBuilder.withPayload(payload).setHeader(HEAD_KEY, channelType).build();
		channel.send(message);
		assertEquals(payload, HeaderService.WEBSERVICE_NAME);
	}
	@Test
	public void testRoute4() {
		String payload = "valuesimple";
		String channelType = "simple";
		Message<String> message = MessageBuilder.withPayload(payload).setHeader(HEAD_KEY, channelType).build();
		channel.send(message);
		assertEquals(payload, HeaderService.SIMPLE_NAME);
	}

	
}
