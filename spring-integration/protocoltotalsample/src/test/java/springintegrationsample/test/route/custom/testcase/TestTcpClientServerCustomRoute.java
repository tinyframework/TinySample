package springintegrationsample.test.route.custom.testcase;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.ip.tcp.connection.AbstractServerConnectionFactory;
import org.springframework.integration.ip.util.TestingUtilities;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import springintegrationsample.CustomTestContextLoader;
import springintegrationsample.FixGateWay;
import springintegrationsample.test.util.DataUtil;

import com.hundsun.fasp.trans.fix.base.FixData;

/**
 * 自定义route的例子
 * route方案是CustomFixDataRouter
 * @author chenjiao
 *
 */
@ContextConfiguration(loader = CustomTestContextLoader.class, locations = { "/route/tcpClientServerRoute.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
public class TestTcpClientServerCustomRoute {
	@Autowired
	FixGateWay gw;

	@Autowired
	AbstractServerConnectionFactory serverConnectionFactory;

	@Before
	public void setup() {
		TestingUtilities.waitListening(this.serverConnectionFactory, 10000000L);
	}

	@Test
	public void testRouteService1() {
		
		FixData result = gw.send(DataUtil.getFixData(1));
		
		System.out.println(result);
		assertEquals(5, result.getColumnCount());
		int index = result.getRowCount() - 1;
		Map<String, Object> map = result.getValues().get(index);
		assertEquals("Route1Service", map.get("a"));

	}

	@Test
	public void testRouteService2() {
		FixData result = gw.send(DataUtil.getFixData(2));
		System.out.println(result);
		assertEquals(5, result.getColumnCount());
		int index = result.getRowCount() - 1;
		Map<String, Object> map = result.getValues().get(index);
		assertEquals("Route2Service", map.get("a"));

	}

	@Test
	public void testRouteService3() {
		FixData result = gw.send(DataUtil.getFixData(3));
		System.out.println(result);
		assertEquals(5, result.getColumnCount());
		int index = result.getRowCount() - 1;
		Map<String, Object> map = result.getValues().get(index);
		assertEquals("Route3Service", map.get("a"));

	}

}
