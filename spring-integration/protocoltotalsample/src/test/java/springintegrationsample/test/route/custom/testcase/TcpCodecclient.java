/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package springintegrationsample.test.route.custom.testcase;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import springintegrationsample.CustomTestContextLoader;
import springintegrationsample.FixGateWay;

import com.hundsun.fasp.trans.fix.base.FixData;



/**
 *  尚未完成。
 * @author chenjiao
 *
 */
@ContextConfiguration(loader=CustomTestContextLoader.class, locations={"/fix/tcpCodecclient.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
public class TcpCodecclient {

	@Autowired
	FixGateWay gw;


	@Test
	public void testHappyDay() {
		FixData result = gw.send( getFixData());
		System.out.println("================");
		System.out.println(result);
		System.out.println("================");
		assertEquals(5, result.getColumnCount());
		
		
	}
	
	public FixData getFixData(){
		FixData fd = new FixData();
		fd.addColumn("a", "String", true);
		fd.addColumn("b", "String", true);
		fd.addColumn("c", "String", true);
		fd.addColumn("d", "String", true);
		fd.addColumn("e", "String", true);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("a", "1");
		map.put("b", "1");
		map.put("c", "1");
		map.put("d", "1");
		map.put("e", "1");
		fd.addValue(map);
		return fd;
	}

}
