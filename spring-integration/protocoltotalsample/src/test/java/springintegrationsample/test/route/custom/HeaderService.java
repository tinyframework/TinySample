package springintegrationsample.test.route.custom;

public class HeaderService {
	
	public static String TCP_NAME = null;
	public static String MQ_NAME = null;
	public static String WEBSERVICE_NAME = null;
	public static String SIMPLE_NAME = null;
	
	public  void tcp(String name){
		TCP_NAME = name;
	}
	public void mq(String name){
		MQ_NAME = name;
	}
	
	public void webservice(String name){
		WEBSERVICE_NAME = name;
	}
	
	public void simple(String name){
		SIMPLE_NAME = name;
	}
}
