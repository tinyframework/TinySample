package springintegrationsample.test.route.custom;

import java.util.HashMap;
import java.util.Map;

import com.hundsun.fasp.trans.fix.base.FixData;

public class Route2Service {
	public FixData helloFixData(FixData fix){
		System.out.println("---------Route2Service-----------");
		Map<String, Object> value  = new HashMap<String, Object>();
		String valueString = "Route2Service";
		value.put("a", valueString);
		value.put("b", valueString);
		value.put("c", valueString);
		value.put("d", valueString);
		value.put("e", valueString);
		fix.addValue(value);
		return fix;
	}
}
