package springintegrationsample.test.route.custom;

import java.util.Map;

import com.hundsun.fasp.trans.fix.base.FixData;

public class CustomFixDataRouter {
	public String route(FixData payload) {
		Map<String,Object> map = payload.getValues().get(0);
		int i = Integer.parseInt((String) map.get("a") );
		return "route"+i+"ServiceChannel";
	}
}
