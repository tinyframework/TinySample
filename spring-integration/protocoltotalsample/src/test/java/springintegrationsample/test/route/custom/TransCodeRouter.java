package springintegrationsample.test.route.custom;

import java.util.HashMap;
import java.util.Map;

import com.hundsun.fasp.trans.fix.base.FixData;

public class TransCodeRouter {
	/**
	 * 这个数据应该是来自一个配置文件的。开发者需要自己再应用启动时，配置好功能号和功能号使用的通道
	 */
	Map<String,String> transCodeToChannel = new HashMap<String, String>();
	
	public String route(FixData payload) {
		Map<String,Object> map = payload.getValues().get(0);
		String transCode = (String) map.get("transCode") ;
		return transCodeToChannel.get(transCode);
	}
}
