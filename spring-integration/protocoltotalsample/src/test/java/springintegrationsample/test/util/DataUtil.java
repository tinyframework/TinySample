package springintegrationsample.test.util;


import java.util.HashMap;
import java.util.Map;

import com.hundsun.fasp.trans.fix.base.FixData;

public class DataUtil {
	public static FixData getFixData(int i){
		
		FixData fd = new FixData();
		fd.addColumn("a", "String", true);
		fd.addColumn("b", "String", true);
		fd.addColumn("c", "String", true);
		fd.addColumn("d", "String", true);
		fd.addColumn("e", "String", true);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("a", ""+i);
		map.put("b", "1");
		map.put("c", "1");
		map.put("d", "1");
		map.put("e", "1");
		fd.addValue(map);
		return fd;
	}

	
}
