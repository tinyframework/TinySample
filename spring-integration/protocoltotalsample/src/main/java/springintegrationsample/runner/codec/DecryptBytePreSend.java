package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

public class DecryptBytePreSend extends BaseSerializerInterceptor {
	

	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		byte[] pack = (byte[])message.getPayload();
		try {
			return new GenericMessage<byte[]>(sm4.decrypt(pack), message.getHeaders());
//			return MessageBuilder.withPayload(sm4.decrypt(pack)).build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
