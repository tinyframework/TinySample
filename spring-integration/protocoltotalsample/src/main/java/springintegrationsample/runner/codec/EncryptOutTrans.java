package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

public class EncryptOutTrans {
	public Message<byte[]> decrypt(Message<byte[]> message) {
		byte[] pack = message.getPayload();
		try {
			byte[] decryptpack = BaseSerializerInterceptor.sm4.decrypt(pack);
			System.out.println("----------------------");
			System.out.println("decrypt:");
			System.out.println(pack.length+"->"+decryptpack.length);
			System.out.println("----------------------");
			return new GenericMessage<byte[]>(decryptpack, message.getHeaders());
//			return MessageBuilder.withPayload(sm4.decrypt(pack)).build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

}

public Message<byte[]> encrypt(Message<byte[]> message) {
	byte[] pack = message.getPayload();
	try {
		byte[] encrypt = BaseSerializerInterceptor.sm4.encrypt(pack);
		System.out.println("----------------------");
		System.out.println("decrypt:");
		System.out.println(pack.length+"->"+encrypt.length);
		System.out.println("----------------------");
		return new GenericMessage<byte[]>(encrypt, message.getHeaders());
//		return MessageBuilder.withPayload(sm4.decrypt(pack)).build();
	} catch (Exception e) {
		throw new RuntimeException(e);
	}
}
}
