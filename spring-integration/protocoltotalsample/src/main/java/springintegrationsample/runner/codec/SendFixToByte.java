package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import com.hundsun.fasp.trans.fix.base.FixData;

public class SendFixToByte extends BaseSerializerInterceptor {
	

	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		if (message.getPayload() instanceof FixData) {
			FixData fix = (FixData)message.getPayload();
			try {
				byte[] data = make(fix);
				return new GenericMessage<byte[]>(data, message.getHeaders());
//				return copy(message,MessageBuilder.fromMessage(message).copyHeaders(message.getHeaders()).withPayload(data).build());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return message;
	}

}
