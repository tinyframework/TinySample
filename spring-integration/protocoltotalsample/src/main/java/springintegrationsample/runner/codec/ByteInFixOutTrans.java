package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;

import com.hundsun.fasp.trans.fix.base.FixData;

public class ByteInFixOutTrans {
	public FixData transbytesToFixData(Message<byte[]> message) {
			byte[] pack = message.getPayload();
			try {
				System.out.println("----------------------");
				System.out.println("transbytesToFixData");
				System.out.println(pack.length);
				System.out.println("----------------------");
				
				FixData fix = BaseSerializerInterceptor.paser(pack);
//				return new GenericMessage<FixData>(fix, message.getHeaders());
				return fix;
//				Message<FixData> target = MessageBuilder.fromMessage(message).withPayload(fix).build();
//				return copy(message, target);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

	}

	public byte[] transFixTobytes(Message<FixData> message) {
		FixData fix = (FixData)message.getPayload();
		try {
			byte[] data = BaseSerializerInterceptor.make(fix);
//			return new GenericMessage<byte[]>(data, message.getHeaders());
			return data;
//			return copy(message,MessageBuilder.fromMessage(message).withPayload(data).build());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
