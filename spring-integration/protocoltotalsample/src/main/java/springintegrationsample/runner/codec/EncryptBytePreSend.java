package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

public class EncryptBytePreSend extends BaseSerializerInterceptor {
	

	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		byte[] pack = (byte[])message.getPayload();
		try {
			return new GenericMessage<byte[]>(sm4.encrypt(pack), message.getHeaders());
//			return MessageBuilder.withPayload(sm4.encrypt(pack)).build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
