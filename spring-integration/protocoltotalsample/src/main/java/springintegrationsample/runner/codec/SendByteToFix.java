package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import com.hundsun.fasp.trans.fix.base.FixData;

public class SendByteToFix extends BaseSerializerInterceptor {

	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		if (message.getPayload() instanceof byte[]) {
			byte[] pack = (byte[]) message.getPayload();
			try {
				FixData fix = paser(pack);
				return new GenericMessage<FixData>(fix, message.getHeaders());
//				Message<FixData> target = MessageBuilder.fromMessage(message).withPayload(fix).build();
//				return copy(message, target);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}
		return message;
	}

}
