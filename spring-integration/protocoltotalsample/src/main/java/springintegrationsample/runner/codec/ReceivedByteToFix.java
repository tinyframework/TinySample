package springintegrationsample.runner.codec;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import com.hundsun.fasp.trans.fix.base.FixData;

public class ReceivedByteToFix extends BaseSerializerInterceptor {
	

	public Message<?> preReceive(Message<?> message, MessageChannel channel) {
		if (message.getPayload() instanceof byte[]) {
			byte[] pack = (byte[])message.getPayload();
			try {
				FixData fix = paser(pack);
				return new GenericMessage<FixData>(fix, message.getHeaders());
//				return copy(message,MessageBuilder.fromMessage(message).withPayload(fix).build());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return message;
	}

}
