package springintegrationsample.runner;

public interface Encrypter {
	/**
     * 加密信息
     * @param sourceData
     * @return
     * @throws GeneralSecurityException
     */
    public byte[] encrypt(byte[] sourceData);

    /**
     * 加密信息
     * @param sourceData
     * @return
     * @throws GeneralSecurityException
     */
    public String encrypt(String sourceData);

    /**
     * 解密信息
     * @param encryptedData
     * @return
     * @throws GeneralSecurityException
     */
    public byte[] decrypt(byte[] encryptedData);

    /**
     * 解密信息
     * @param encryptedData
     * @return
     * @throws GeneralSecurityException
     */
    public String decrypt(String encryptedData);
}
