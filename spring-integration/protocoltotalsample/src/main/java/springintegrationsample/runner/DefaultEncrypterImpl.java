package springintegrationsample.runner;

import org.tinygroup.chinesecipher.SM4;
import org.tinygroup.chinesecipher.exception.ChineseCipherException;
import org.tinygroup.chinesecipher.sm4.ECBSM4;

public class DefaultEncrypterImpl implements Encrypter {
	static SM4 sm4 = null;
	
	static{
		try {
			sm4 = new ECBSM4("JeF8U9wHFOMfs2Y8");
		} catch (ChineseCipherException e) {
			e.printStackTrace();
		}
	}
	@Override
	public byte[] encrypt(byte[] sourceData) {
		try {
			return sm4.encrypt(sourceData);
		} catch (ChineseCipherException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String encrypt(String sourceData) {
		try {
			return sm4.encrypt(sourceData);
		} catch (ChineseCipherException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] encryptedData) {
		try {
			return sm4.decrypt(encryptedData);
		} catch (ChineseCipherException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String decrypt(String encryptedData) {
		try {
			return sm4.decrypt(encryptedData);
		} catch (ChineseCipherException e) {
			throw new RuntimeException(e);
		}
	}

}
