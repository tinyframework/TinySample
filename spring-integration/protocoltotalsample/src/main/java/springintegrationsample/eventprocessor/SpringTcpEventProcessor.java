package springintegrationsample.eventprocessor;

import java.util.ArrayList;
import java.util.List;

import org.tinygroup.cepcore.CEPCore;
import org.tinygroup.cepcore.impl.AbstractEventProcessor;
import org.tinygroup.event.Event;
import org.tinygroup.event.ServiceInfo;

import springintegrationsample.FixGateWay;

import com.hundsun.fasp.trans.fix.base.FixData;


public class SpringTcpEventProcessor extends AbstractEventProcessor{
	public static final String BEAN_ID ="springTcpEventProcessor";
	
	
	/**
	 * FIXData的getWay，在这里用于向TCP服务端发送请求
	 */
	private FixGateWay gw;
	
	private boolean read;
	
	private List<ServiceInfo> services = new ArrayList<ServiceInfo>();
	
	/* (non-Javadoc)
	 * @see org.tinygroup.cepcore.EventProcessor#process(org.tinygroup.event.Event)
	 * 
	 */
	public void process(Event event) {
		FixData data = getGw().send((FixData) event.getServiceRequest().getContext().get(SimpleService.PARAM_NAME));
		event.getServiceRequest().getContext().put(SimpleService.RESULT_NAME, data);
	}

	public void setCepCore(CEPCore cepCore) {
		// TODO Auto-generated method stub
		
	}

	public List<ServiceInfo> getServiceInfos() {
		return services;
	}

	public int getWeight() {
		return 0;
	}

	public List<String> getRegex() {
		return null;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public FixGateWay getGw() {
		return gw;
	}

	public void setGw(FixGateWay gw) {
		this.gw = gw;
	}

}
