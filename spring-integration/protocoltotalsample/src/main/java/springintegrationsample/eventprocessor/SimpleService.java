package springintegrationsample.eventprocessor;

import java.util.ArrayList;
import java.util.List;

import org.tinygroup.event.AbstractServiceInfo;
import org.tinygroup.event.Parameter;

public class SimpleService extends AbstractServiceInfo{
	public static final String RESULT_NAME = "result";
	public static final String PARAM_NAME = "data";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1883358779780372175L;
	List<Parameter> params = new ArrayList<Parameter>();
	List<Parameter> result = new ArrayList<Parameter>();
	String serviceId;
	
	
	public SimpleService(String serviceId) {
		this.serviceId = serviceId;
		
		String type = "org.tinygroup.protocol.fix.FixData";
		Parameter p = new Parameter();
		p.setArray(false);
		p.setType(type);
		p.setName(PARAM_NAME);
		params.add(p);
		
		Parameter resultP= new Parameter();
		resultP.setArray(false);
		resultP.setType(type);
		resultP.setName(RESULT_NAME);
		result.add(resultP);
	}

	public String getServiceId() {
		return serviceId;
	}

	public List<Parameter> getParameters() {
		return params;
	}

	public List<Parameter> getResults() {
		return result;
	}

	public String getCategory() {
		return null;
	}
	
}
