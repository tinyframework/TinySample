package springintegrationsample;

import com.hundsun.fasp.trans.fix.base.FixData;


public interface FixGateWay {
	public FixData send(FixData text);
}
