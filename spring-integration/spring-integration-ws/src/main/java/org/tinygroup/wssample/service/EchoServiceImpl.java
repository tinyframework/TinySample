package org.tinygroup.wssample.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;

import org.springframework.stereotype.Component;

@Component("echoService")
@WebService(serviceName = "echo")  
@SOAPBinding(parameterStyle=ParameterStyle.WRAPPED)  
public class EchoServiceImpl implements EchoService {
	@WebMethod  
	public String echo(String name) {
		if (name == null || name.trim().length() == 0) {
			return "echo back: -please provide a name-";
		}
		SimpleDateFormat dtfmt = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
		return "echo back: received on "
				+ dtfmt.format(Calendar.getInstance().getTime());
	}

}
