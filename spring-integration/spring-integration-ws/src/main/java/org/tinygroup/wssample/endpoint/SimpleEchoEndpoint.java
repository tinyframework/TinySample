package org.tinygroup.wssample.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.tinygroup.wssample.service.EchoService;

@Endpoint
public class SimpleEchoEndpoint {
	private static final String NAMESPACE = "http://www.upyaya.org/echo/schemas";

	@Autowired
	private EchoService echoService;

	@PayloadRoot(localPart = "echo", namespace = NAMESPACE)
	public @ResponsePayload String echo(String echo) {
		return echoService.echo(echo);
	}

}
