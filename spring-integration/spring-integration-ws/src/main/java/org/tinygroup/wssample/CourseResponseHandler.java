package org.tinygroup.wssample;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.wssample.types.GetCourseResponse;

@Component
public class CourseResponseHandler {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String getResponse(Message<GetCourseResponse> msg) {
		GetCourseResponse course = msg.getPayload();
		logger.info("Course with ID [{}] received: {}", course.getCourseId(),
				course.getName());

		return course.getName();
	}
}