package org.tinygroup.wssample.gateway;

/**
 * 客户端调用webservice服务的入口网关
 * @author renhui
 *
 */
public interface GetCourseService {
	public String findCourse(String courseId);
}
