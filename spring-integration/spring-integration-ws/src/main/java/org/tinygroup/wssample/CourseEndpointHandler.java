package org.tinygroup.wssample;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tinygroup.wssample.service.CourseService;
import org.tinygroup.wssample.service.exception.CourseNotFoundException;
import org.tinygroup.wssample.types.Course;
import org.tinygroup.wssample.types.GetCourseListRequest;
import org.tinygroup.wssample.types.GetCourseListResponse;
import org.tinygroup.wssample.types.GetCourseRequest;
import org.tinygroup.wssample.types.GetCourseResponse;

@Component
public class CourseEndpointHandler {

	@Autowired
	private CourseService service;

	public GetCourseResponse getCourse(GetCourseRequest request) {
		Course course = service.getCourse(request.getCourseId());
		if (course == null) {
			throw new CourseNotFoundException("course ["
					+ request.getCourseId() + "] does not exist");
		}

		GetCourseResponse response = new GetCourseResponse();
		response.setCourseId(course.getCourseId());
		response.setDescription(course.getDescription());
		response.setName(course.getName());
		response.setSubscriptors(course.getSubscriptors());

		return response;
	}

	public Course handle(Course course) {
		System.out.println(course.getCourseId());
		System.out.println(course.getName());
		course.setCourseId("ddsdsf");
		;
		course.setName("Hello Spring!");
		return course;
	}

	public GetCourseListResponse getCourseList(GetCourseListRequest request) {
		GetCourseListResponse response = new GetCourseListResponse();
		for (Map.Entry<String, Course> entry : service.getCourses().entrySet()) {
			response.getCourse().add(entry.getValue());
		}

		return response;
	}

}
