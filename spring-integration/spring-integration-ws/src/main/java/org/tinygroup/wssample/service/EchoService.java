package org.tinygroup.wssample.service;

public interface EchoService {
	public String echo(String name);
}
