package org.tinygroup.wssample.service;

import java.util.Map;

import org.tinygroup.wssample.types.Course;

public interface CourseService {

	Map<String, Course> getCourses();
	
	Course getCourse(String courseId);
}
