package org.tinygroup.wssample.transform;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.wssample.types.GetCourseRequest;

@Component
public class CourseRequestBuilder {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public GetCourseRequest buildRequest(Message<String> msg) {
		logger.info("Building request for course [{}]", msg.getPayload());
		GetCourseRequest request = new GetCourseRequest();
		request.setCourseId(msg.getPayload());
		return request;
	}
}