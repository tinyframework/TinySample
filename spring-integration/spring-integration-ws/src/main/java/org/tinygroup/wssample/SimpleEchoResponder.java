package org.tinygroup.wssample;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.springframework.integration.xml.source.DomSourceFactory;

public class SimpleEchoResponder {

	public Source issueResponseFor(DOMSource request) {
		return new DomSourceFactory().createSource(
				"<echoResponse xmlns=\"http://www.upyaya.org/echo/schemas\">" +
				request.getNode().getTextContent() + "</echoResponse>");
	}
}