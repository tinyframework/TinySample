package org.tinygroup.wssample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tinygroup.wssample.gateway.GetCourseService;
import org.tinygroup.wssample.types.GetCourseRequest;

/**
 * 运行此测试用例之前需要把本工程以web应用方式启动，进行发布webservice服务，然后再运行此测试用例。
 * 
 * @author renhui
 *
 */
@ContextConfiguration({ "classpath:/META-INF/spring/integration/client.xml" })
//@RunWith(SpringJUnit4ClassRunner.class)
public class TestCourseApp {

	@Autowired
	GetCourseService getCourseService;

//	@Test
	public void invokeGetCourseOperation() throws Exception {
		GetCourseRequest request = new GetCourseRequest();
		request.setCourseId("BC-45");

		String courseName = getCourseService.findCourse("BC-45");

		assertEquals("Introduction to Java", courseName);
	}

}
