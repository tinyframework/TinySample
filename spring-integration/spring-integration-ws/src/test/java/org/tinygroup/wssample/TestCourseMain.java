package org.tinygroup.wssample;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tinygroup.wssample.gateway.GetCourseService;
import org.tinygroup.wssample.types.GetCourseRequest;

public class TestCourseMain {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:/META-INF/spring/integration/client.xml");
		GetCourseService getCourseService = context
				.getBean(GetCourseService.class);

		GetCourseRequest request = new GetCourseRequest();
		request.setCourseId("BC-45");

		String courseName = getCourseService.findCourse("BC-45");

		System.out.println(courseName);
		
		context.close();

	}

}
