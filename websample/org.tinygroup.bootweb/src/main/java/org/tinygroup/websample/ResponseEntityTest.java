package org.tinygroup.websample;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tinygroup.crud.pojo.User;

@Controller
@RequestMapping("/user")
public class ResponseEntityTest {

	 @RequestMapping("/responseEntity")
	  public ResponseEntity<User> responseEntity(){
		  User user=new User();
		  user.setName("的说法是非法");
		  return new ResponseEntity<User>(user, HttpStatus.OK);
		  
	  }
	
}
