package org.tinygroup.websample;

import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.config.util.ConfigurationUtil;
import org.tinygroup.fileresolver.FileResolverFactory;
import org.tinygroup.fileresolver.FullContextFileRepository;
import org.tinygroup.loader.LoaderManager;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.remoteconfig.utils.TinyConfigParamUtil;
import org.tinygroup.springutil.SpringBootBeanContainer;
import org.tinygroup.vfs.VFS;
import org.tinygroup.weblayer.configmanager.TinyListenerConfigManagerHolder;
import org.tinygroup.weblayer.listener.ServletContextHolder;
import org.tinygroup.weblayer.listener.TinyServletContext;
import org.tinygroup.xstream.XStreamFactory;

public class TinyServletContextListener implements ServletContextListener {

	private static Logger logger = LoggerFactory
			.getLogger(TinyServletContextListener.class);
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		TinyServletContext servletContext = new TinyServletContext(
				servletContextEvent.getServletContext());
		ServletContextHolder.setServletContext(servletContext);
		Enumeration<String> enumeration = servletContextEvent
				.getServletContext().getAttributeNames();
		logger.logMessage(LogLevel.INFO, "WEB环境属性开始");
		while (enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			logger.logMessage(LogLevel.INFO, "{0}=[{1}]", key,
					servletContextEvent.getServletContext().getAttribute(key));
		}
		logger.logMessage(LogLevel.INFO, "WEB 应用启动中...");

		logger.logMessage(LogLevel.INFO, "WEB 应用信息：[{0}]", servletContextEvent
				.getServletContext().getServerInfo());
		String webRootPath = servletContextEvent.getServletContext()
				.getRealPath("/");
		if (webRootPath == null) {
			try {
				webRootPath = servletContextEvent.getServletContext()
						.getResource("/").getFile();
			} catch (MalformedURLException e) {
				logger.errorMessage("获取WEBROOT失败！", e);
			}
		}
		logger.logMessage(LogLevel.INFO, "TINY_WEBROOT：[{0}]", webRootPath);
		ConfigurationUtil.getConfigurationManager().setConfiguration(
				"TINY_WEBROOT", webRootPath);
		logger.logMessage(LogLevel.INFO, "应用参数<TINY_WEBROOT>=<{}>", webRootPath);

		logger.logMessage(LogLevel.INFO, "ServerContextName：[{0}]",
				servletContextEvent.getServletContext().getServletContextName());
		logger.logMessage(LogLevel.INFO, "WEB环境属性结束");
		
		Object rootContext = servletContext.getAttribute(
				WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		if(rootContext!=null){
			BeanContainerFactory.initBeanContainer(SpringBootBeanContainer.class
					.getName());
			SpringBootBeanContainer beanContainer = (SpringBootBeanContainer) BeanContainerFactory
					.getBeanContainer(getClass().getClassLoader());
			beanContainer.setApplicationContext((ApplicationContext) rootContext);
		}
//		FullContextFileRepository fileRepository = BeanContainerFactory
//				.getBeanContainer(this.getClass().getClassLoader())
//				.getBean(
//						FullContextFileRepository.FILE_REPOSITORY_BEAN_NAME);
//		servletContext.setFullContextFileRepository(fileRepository);// 设置上下文关联的全文搜索对象
		initContextListener(servletContextEvent);
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		try {
			logger.logMessage(LogLevel.INFO, "WEB 应用停止中...");
			destroyContextListener(servletContextEvent);
			ConfigurationUtil.clear();
			ServletContextHolder.clear();
			BeanContainerFactory.destroy();
			FileResolverFactory.destroyFileResolver();// 关闭容器的时候设置扫描spring配置文件的扫码器实例为null
			logger.logMessage(LogLevel.INFO, "WEB 远程配置停止中...");
			TinyConfigParamUtil.setReadClient(null);
			logger.logMessage(LogLevel.INFO, "WEB 远程配置停止完成。");
			VFS.clearCache();
			LoggerFactory.clearAllLoggers();
			LoaderManager.clear();
			XStreamFactory.clear();
			LogFactory.release(Thread.currentThread().getContextClassLoader());
			logger.logMessage(LogLevel.INFO, "WEB 应用停止完成。");
		} catch (Exception e) {
			logger.errorMessage("tiny应用关闭出错：", e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 执行其他ContextListener
	 */
	private void initContextListener(ServletContextEvent servletContextEvent) {
		List<ServletContextListener> contextListeners = TinyListenerConfigManagerHolder
				.getInstance().getContextListeners();
		for (ServletContextListener servletContextListener : contextListeners) {
			logger.logMessage(LogLevel.DEBUG,
					"ServletContextListener:[{0}] will be Initialized",
					servletContextListener);
			servletContextListener.contextInitialized(servletContextEvent);
			logger.logMessage(LogLevel.DEBUG,
					"ServletContextListener:[{0}] Initialized",
					servletContextListener);
		}
	}

	/**
	 * 执行其他ContextListener
	 */
	private void destroyContextListener(ServletContextEvent servletContextEvent) {
		List<ServletContextListener> contextListeners = TinyListenerConfigManagerHolder
				.getInstance().getContextListeners();
		for (ServletContextListener servletContextListener : contextListeners) {
			logger.logMessage(LogLevel.DEBUG,
					"ServletContextListener:[{0}] will be Destroyed",
					servletContextListener);
			servletContextListener.contextDestroyed(servletContextEvent);
			logger.logMessage(LogLevel.DEBUG,
					"ServletContextListener:[{0}] Destroyed",
					servletContextListener);
		}
	}

}
