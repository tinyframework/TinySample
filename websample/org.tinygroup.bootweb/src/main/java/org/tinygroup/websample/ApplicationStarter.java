package org.tinygroup.websample;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.filter.OrderedCharacterEncodingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.orm.hibernate5.support.OpenSessionInViewFilter;
import org.tinygroup.weblayer.ApplicationStartupListener;
import org.tinygroup.weblayer.TinyHttpFilter;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,ValidationAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class, // （如果使用Hibernate时，需要加）
		DataSourceTransactionManagerAutoConfiguration.class, })
@ComponentScan(basePackages = { "org.tinygroup" })
public class ApplicationStarter extends SpringBootServletInitializer {
 
	
	@Bean
	public ServletListenerRegistrationBean<ApplicationStartupListener> tinyServletListenerRegistrationBean() {
		return new ServletListenerRegistrationBean<ApplicationStartupListener>(
				new ApplicationStartupListener());
	}
	/*@Bean
	public ServletListenerRegistrationBean<TinyServletContextListener> tinyServletListenerRegistrationBean() {
		return new ServletListenerRegistrationBean<TinyServletContextListener>(
				new TinyServletContextListener());
	}*/

	@Bean
	public FilterRegistrationBean openSessionInViewFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(
				new OpenSessionInViewFilter());
		filterRegistrationBean.addInitParameter("flushMode", "AUTO");
		return filterRegistrationBean;
	}

	@Bean
	public OrderedCharacterEncodingFilter encodingFilter() {
		OrderedCharacterEncodingFilter encodingFilter = new OrderedCharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		encodingFilter.setForceRequestEncoding(true);
		encodingFilter.setForceResponseEncoding(true);
		encodingFilter.setOrder(Integer.MIN_VALUE);
		return encodingFilter;
	}

	 @Bean
	 public FilterRegistrationBean tinyFilterRegistrationBean(){
	 FilterRegistrationBean filterRegistrationBean = new
	 FilterRegistrationBean(new TinyHttpFilter());
	 filterRegistrationBean.addInitParameter("excludePath", ".*/services.*");
	 filterRegistrationBean.addInitParameter("passthruPath", "/index.html");
	 return filterRegistrationBean;
	 }

	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(ApplicationStarter.class);
	}

	public static void main(String[] vars) {
		String[] args = (String[]) ArrayUtils.addAll(vars,
				new String[] { "--spring.config.name=framework" });
		Object[] sources = new Object[3];
		sources[0] = ApplicationStarter.class;
		sources[1] = "classpath*:**/*.beans.xml";
		sources[2] = "classpath*:**/mvc-beans.xml";
		SpringApplication application = new SpringApplication(sources);
		application.run(args); 

	}
}