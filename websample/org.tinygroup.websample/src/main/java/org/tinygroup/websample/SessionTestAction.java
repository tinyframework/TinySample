package org.tinygroup.websample;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tinygroup.cache.Cache;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.crud.pojo.User;
import org.tinygroup.weblayer.WebContext;
import org.tinygroup.weblayer.webcontext.util.WebContextUtil;

@Controller
@RequestMapping("/session")
public class SessionTestAction {
	
	@Value("${cache_region}")
	private String value;

	@Resource(name="cacheBean")
	private Cache cache;
	
	
	@RequestMapping("/setAttribute")
	@ResponseBody
	public String setAttribute(String value,HttpServletRequest request){
		request.getSession().setAttribute("test", value);
		User user = new User();
		user.setId("2233");
		user.setName("fdfdf");
		user.setAge(10);
		request.getSession().setAttribute("user",user);
		return "{setAttribute:"+value+"}";
	}
	
	@RequestMapping("/getAttribute")
	@ResponseBody
	public String getAttribute(HttpServletRequest request,WebContext webContext){
		Object value=request.getSession().getAttribute("test");
		System.out.println(value);
		return "{getAttribute:"+value+"}";
	}
	
	@RequestMapping("/getAttribute1")
	@ResponseBody
	public String getAttribute1(HttpServletRequest request,WebContext webContext){
		Object value=request.getSession().getAttribute("test");
		System.out.println(value);
		return "{getAttribute:"+value+"}";
	}
	
	@RequestMapping("/removeAttribute")
	@ResponseBody
	public String removeAttribute(HttpServletRequest request){
		request.getSession().removeAttribute("test");
		return "{removeAttribute:test}";
	}
	
	
	@RequestMapping(value = { "/test/{id}.html" })
	public void testListenerMethod1(@PathVariable("id") String id) {
		System.out.println(id);
	}
	
	@RequestMapping(value = { "/test/**" })
	@ResponseBody
	public String testListenerMethod2(HttpServletRequest request) {
		String url=WebContextUtil.getServletPath(request)+"?" + request.getQueryString();
		System.out.println(url);
		String value = StringUtil.substringAfter(url, "test");
		System.out.println(value);
		request.setAttribute("urlparam", value);
		return value;
	}
}
