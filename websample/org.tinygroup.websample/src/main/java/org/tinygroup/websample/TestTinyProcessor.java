package org.tinygroup.websample;

import java.io.IOException;

import javax.servlet.ServletException;

import org.tinygroup.commons.io.StreamUtil;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.weblayer.AbstractTinyProcessor;
import org.tinygroup.weblayer.WebContext;

public class TestTinyProcessor extends AbstractTinyProcessor {

	@Override
	protected void customInit() throws ServletException {

	}

	@Override
	public void reallyProcess(String urlString, WebContext context)
			throws ServletException, IOException {
		byte[] data = StreamUtil.readBytes(
				context.getRequest().getInputStream(), true).toByteArray();
		String characterEncoding = context.getRequest().getCharacterEncoding();
		if(StringUtil.isBlank(characterEncoding)){
			characterEncoding = "UTF-8";
		}
		String inputXml = new String(data, characterEncoding);
		System.out.println(inputXml);
	}

}
