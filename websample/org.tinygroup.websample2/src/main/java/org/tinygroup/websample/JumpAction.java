package org.tinygroup.websample;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/jump")
public class JumpAction {
	
	
	@RequestMapping("/forward")
	public void forward(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher("/session/getAttribute").forward(request, response);
	}
	
	@RequestMapping("/redirect")
	public void redirect(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		response.sendRedirect(request.getSession().getServletContext().getContextPath()+"/session/getAttribute");
	}
	
	@RequestMapping("/sendError")
	public void sendError(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

}
