package org.tinygroup.websample;

import java.io.IOException;

import javax.servlet.ServletException;

import org.tinygroup.weblayer.WebContext;
import org.tinygroup.weblayer.exceptionhandler.WebExceptionHandler;

/**
 * 
 * @author renhui
 *
 */
public class ForwardWebExceptionHandler implements WebExceptionHandler {

	@Override
	public void handler(Throwable throwable, WebContext webContext)
			throws IOException, ServletException {
		webContext.getRequest().getRequestDispatcher("/error/error.html")
				.forward(webContext.getRequest(), webContext.getResponse());
	}

}
