package org.tinygroup.websample;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/patch")
public class PatchAction {

	@RequestMapping(value = "/fromPatch", method = RequestMethod.PATCH)
	public @ResponseBody String patchRequest() {
		return "from patch";
	}
	
	@RequestMapping(value = "/fromPatch", method = RequestMethod.GET)
	public @ResponseBody String patchGet() {
		return "from get";
	}

}
