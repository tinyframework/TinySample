package org.tinygroup.websample;

import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/redirectAttributes")
public class RedirectAttributesTest {

	@RequestMapping(value = "openAccount")
	public String openAccount(RedirectAttributes reAttributes) {

		reAttributes.addAttribute("value", UUID.randomUUID());
		return "redirect:/session/setAttribute";
	}

}
