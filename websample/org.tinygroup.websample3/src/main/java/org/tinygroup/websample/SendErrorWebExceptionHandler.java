package org.tinygroup.websample;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.tinygroup.weblayer.WebContext;
import org.tinygroup.weblayer.exceptionhandler.WebExceptionHandler;

public class SendErrorWebExceptionHandler implements WebExceptionHandler {

	@Override
	public void handler(Throwable throwable, WebContext webContext)
			throws IOException, ServletException {
           webContext.getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
	}

}
