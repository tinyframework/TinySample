package org.tinygroup.websample;

import java.lang.annotation.Annotation;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tinygroup.cache.Cache;
import org.tinygroup.commons.tools.AnnotationUtils;
import org.tinygroup.weblayer.WebContext;

@Controller
@RequestMapping("/session")
public class SessionTestAction {
	
	@Value("${cache_region}")
	private String value;

	@Resource(name="cacheBean")
	private Cache cache;
	
	
	@RequestMapping("/setAttribute")
	@ResponseBody
	public String setAttribute(String value,HttpServletRequest request){
		request.getSession().setAttribute("test", value);
		return "{setAttribute:"+value+"}";
	}
	
	@RequestMapping("/getAttribute")
	@ResponseBody
	public String getAttribute(HttpServletRequest request,WebContext webContext){
		Object value=request.getSession().getAttribute("test");
		System.out.println(webContext.get("test"));
		System.out.println(value);
		String sessionId=request.getSession().getId();
		System.out.println(sessionId);
		System.out.println(cache.getGroupKeys(sessionId));
		return "{getAttribute:"+value+"}";
	}
	
	@RequestMapping("/removeAttribute")
	@ResponseBody
	public String removeAttribute(HttpServletRequest request){
		request.getSession().removeAttribute("test");
		return "{removeAttribute:test}";
	}
	
	@RequestMapping("/getCacheValue")
	@ResponseBody
	public String getCacheValue(HttpServletRequest request){
	   return "{getCacheValue:"+cache.get("worktime").toString()+"}";
	}
	
	
	public static void main(String[] args) {
		
		Annotation annotation=AnnotationUtils.findAnnotation(SessionTestAction.class, RequestMapping.class);
	    System.out.println(annotation.annotationType());
		
		
	}
}
