package org.tinygroup.websample;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectJAdvice {

	@Before("execution(* org.tinygroup.websample.SessionTestAction.responseBody(..))")  
	public void beforeAdvice(JoinPoint joinPoint) {  
	    System.out.println("Before: " + joinPoint.getSignature().getName());  
	}  
}