/**
 * 
 */
package org.tinygroup.springbootsample;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yanwj06282
 *
 */
@SpringBootApplication
public class TestSpringbootListener {

	/**
	 * @param args
	 */
	public static void main(String[] vrgs) {
		String[] args = (String[]) ArrayUtils.addAll(vrgs,
				new String[] { "--spring.config.name=tinyconfig" });
		SpringApplication.run(new Object[]{TestSpringbootListener.class ,"classpath*:**/*.beans.xml"} ,args);
	}

}
