/**
 *
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
package org.mytiny.zookeeper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.tinygroup.remoteconfig.zk.utils.SerializeUtil;

/**
 * 
 * @author zhangliang08072
 * @version $Id: ZookeeperUtil.java, v 0.1 2016年5月20日 上午10:04:09 zhangliang08072
 *          Exp $
 */
public class ZookeeperUtil {

	protected static ZooKeeper zooKeeper;

	public static void startZookeeper(String url) {
		if (zooKeeper == null) {
			try {
				zooKeeper = new ZooKeeper(url, 2000, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void stopZookeeper() {
		if (zooKeeper != null) {
			try {
				zooKeeper.close();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void setNode(String key, String value) {
		try {
			Stat stat = zooKeeper.exists(key, false);
			if (stat == null) {
				zooKeeper.create(key, value.getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			} else {
				zooKeeper.setData(key, value.getBytes(), stat.getVersion());
			}
		} catch (KeeperException e) {
			//当没有找到子项时，会抛出此异常，我们不做任何处理
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getNode(String key) {
		try {
			Stat stat = zooKeeper.exists(key, false);
			if (stat != null) {
				byte[] resultData = zooKeeper.getData(key, false, stat);
				if (resultData != null) {
					Object obj = SerializeUtil.unserialize(resultData);
					if (obj == null) {
						return new String(resultData);
					} else if (obj instanceof String) {
						return obj.toString();
					}
				}
			}
		} catch (KeeperException e) {
			//当没有找到子项时，会抛出此异常，我们不做任何处理
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Map<String,String> getAllNode(String nodePath){
		Map<String, String> dataMap = new HashMap<String, String>();
		try {
			List<String> subNodes = zooKeeper.getChildren(nodePath, false);
			if (subNodes != null) {
				for (String subNode : subNodes) {
					String znodeValue = getNode(nodePath.concat("/").concat(subNode));
					if (znodeValue != null) {
						dataMap.put(subNode, znodeValue);
					}
				}
			}
		} catch (KeeperException e) {
			//当没有找到子项时，会抛出此异常，我们不做任何处理
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return dataMap;
	}
	
	public static void main(String[] args) {
		startZookeeper("127.0.0.1:8000");
		setNode("/remoteconfig","真正的缺省根");
		setNode("/remoteconfig/root","根节点");
		setNode("/remoteconfig/root/project1","根节点/project1");
		setNode("/remoteconfig/root/project2","根节点/project2");
		
		setNode("/remoteconfig/root/project2/version1","根节点/project2/version1");
		setNode("/remoteconfig/root/project2/version2","根节点/project2/version2");
		
		Map<String,String> map = getAllNode("/");
		for(Map.Entry<String, String> entry:map.entrySet()){
			System.out.println("====>"+entry.getKey()+" : "+entry.getValue());
		}
		stopZookeeper();
	}
}
