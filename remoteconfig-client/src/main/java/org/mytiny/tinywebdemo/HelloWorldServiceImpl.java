/**
 *
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
package org.mytiny.tinywebdemo;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tinygroup.config.util.ConfigurationUtil;

/**
 * 
 * @author zhangliang08072
 * @version $Id: HelloWorldServiceImpl.java, v 0.1 2016年5月12日 上午10:36:01 zhangliang08072 Exp $
 */
@Component("helloWorldService")
public class HelloWorldServiceImpl implements HelloWorldService {

	/** 
	 * @see org.mytiny.tinywebdemo.HelloWorldService#sayHello(java.lang.String)
	 */
	
	@Value("${sys.param}")
	private String param;
	
	
//	public String getParam() {
//		return param;
//	}
//
//	public void setParam(String param) {
//		this.param = param;
//	}

	@Override
	public String sayHello(String name) {
		return "Hello World,"+name+" "+param;
	}

	/** 
	 * @see org.mytiny.tinywebdemo.HelloWorldService#getRemoteConfig()
	 */
	@Override
	public String getRemoteConfig() {
		// TODO Auto-generated method stub
		Map<String,String> map = ConfigurationUtil.getConfigurationManager().getConfiguration();
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, String> entry:map.entrySet()){
		    sb.append(entry.getKey()).append("=").append(entry.getValue()).append("    ");
		}
		System.out.println(sb.toString());
		try {
			Thread.sleep(100);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return sb.toString();
	}
}
