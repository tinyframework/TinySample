/**
 *
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
package org.mytiny.tinywebdemo;

/**
 * 
 * @author zhangliang08072
 * @version $Id: HelloWorldService.java, v 0.1 2016年5月12日 上午10:34:51 zhangliang08072 Exp $
 */
public interface HelloWorldService {

	String sayHello(String name);
	
	String getRemoteConfig();
}
